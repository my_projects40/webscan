import sqlite3
from flask import Flask, request, jsonify
from flask_socketio import SocketIO, send, emit
from flask_cors import CORS
import hashlib
from Dos import Dos
from twilio.rest import Client
from subprocess import Popen, PIPE, STDOUT, call


########################################### APP CONSTS ###########################################
ACCOUNT_DIS = 'AC737812f1d30f4fc3c3647d8f937a6487'
AUTH_TOKEN = 'a0ceab9f4712f75b0b63b6acdf76e7d3'
SERVICE_ID = 'VAda4f4b97c116bf159a5f9cd2457d417c'

app = Flask(__name__)
CORS(app)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app,cors_allowed_origins="*")
'''
import logging
log = logging.getLogger('werkzeug')
log.disabled = True
'''

client = Client(ACCOUNT_DIS, AUTH_TOKEN)
verify = client.verify.services(SERVICE_ID)


########################################### APP ROUTES ###########################################

'''
backend route that checks the log in credentials
'''
@app.route("/login")
def checkLogin():
    username = request.args.get('username')
    password = request.args.get('password')
    
    return {"msg": checkUser(username, password)}
 

'''
backend route that sign up new user with given credentials
'''
@app.route("/signup")
def newUser():
    username = request.args.get('username')
    password = request.args.get('password')
    phone_number = request.args.get('phone')
    
    return {"msg": addUser(username, password, phone_number)}


'''
backend route that gets user info
'''
@app.route("/getInfo")
def scans():
    username = request.args.get('username')
    result = get_scans(username)
    
    return {"counter": int(result[0]), "admin": bool(result[1])}


'''
backend route that checks dos on given url
'''
@app.route("/dos")
def dos_check():
    url = request.args.get('website')
    dos = Dos(url, '')
    
    return {"result": dos.do_job()}


'''
backend route that send reset password code
'''
@app.route("/reset")
def resetPwd():
    username = request.args.get('username')
    phone = request.args.get('phone')
    
    res = resetPassword(username, phone)
    print(res)
    
    return {"result": res}


'''
backend route that confirms reset password code
'''
@app.route("/confirm")
def confirmResetCode():
    code = request.args.get('code')
    phone = request.args.get('phone')
    new_password = request.args.get('pass')
    username = request.args.get('user')
    
    if verify.verification_checks.create(to=phone, code=code).status == 'approved':
        
        updatePassword(username, new_password)
    
        return {"result": "password reseted"}
    else:
        return {"result": "Wrong verifcation code"}





########################################### APP SOCKET ###########################################

@socketio.on("connect")
def connected():
    """event listener when client connects to the server"""
    print(request.sid)
    print("client has connected")
    emit("connect",{"data": f"id: {request.sid} is connected"})


@socketio.on('start_scan')
def handle_message(url):
    """event listener when client types a message"""
    print(f'started scan on {url}:')
    data = ""
    
    cmd = f'python3 Main.py -u {url}'
    print(cmd)
    
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT)
    
    for line in p.stdout:
        emit_type = "data"
        line = line.decode()[:-1]

        #print(line, line[0], line[-1], line[1], line[-2])

        if line[0] == '[' and line[-1] == ']' and line[1] == '{' and line[-2] == '}':
            
            emit_type = "done"
        
        emit(emit_type, f"{line}", broadcast=True)
        #print(line.decode()[:-1])

@socketio.on("disconnect")
def disconnected():
    """event listener when client disconnects to the server"""
    print("user disconnected")
    emit("disconnect",f"user {request.sid} disconnected",broadcast=True)



########################################### helping functions ###########################################

'''
Function adds given user credentials to the db
Output: string that says if the operation succeeded
'''
def addUser(username: str, password: str, phone_number: str):
    sqliteConnection = sqlite3.connect('sqlite.db')
    cursor = sqliteConnection.cursor()

    hash = hashlib.md5()
    hash.update(password.encode("utf-8"))
    hash = hash.hexdigest()

    print(hash)
    
    print(username, phone_number)
    
    try:
        cursor.execute("INSERT INTO users (username, password, phone, scans, admin) VALUES (?, ?, ?, ?, ?);", (username, hash, phone_number, 1000, 0))
        
        sqliteConnection.commit()
        sqliteConnection.close()
        
        return "User added"
    
    except sqlite3.Error as error:
        print(error)
        cursor.close()
        return "User not added"


'''
Function checks user credentials to the ones in the db
Output: string that says if the operation succeeded
'''
def checkUser(username: str, password: str):
    sqliteConnection = sqlite3.connect('sqlite.db')
    cursor = sqliteConnection.cursor()

    hash = hashlib.md5()
    hash.update(password.encode("utf-8"))
    hash = hash.hexdigest()

    print(hash)
    
    res = cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", (username, hash)).fetchone()
    
    print(res)
    
    sqliteConnection.close()
    
    if res is None:
        return "User invalid"
    
    if res[-1] == 1:
        return "Admin"
    
    return "User valid"



'''
Function update all users 
Output: json of all users
'''
@app.route('/api/users/<int:user_id>', methods=['PUT'])
def update_user(user_id):

    conn = sqlite3.connect('sqlite.db')
    c = conn.cursor()

    updated_user = request.json

    c.execute("UPDATE users SET username=?, password=?, phone=?, scans=?, admin=? WHERE id=?",
              (updated_user['username'], updated_user['password'], updated_user['phone'],
               updated_user['scans'], updated_user['admin'], user_id))
    conn.commit()

    conn.close()

    return jsonify({'message': 'User updated successfully'})


'''
Function get all users 
Output: json of all users
'''
@app.route('/api/users')
def get_users():
    sqliteConnection = sqlite3.connect('sqlite.db')
    cursor = sqliteConnection.cursor()

    res = cursor.execute("SELECT * FROM users").fetchall()

    users = []
    for row in res:
        user = {
            'id': row[0],
            'username': row[1],
            'password': row[2],
            'phone': row[3],
            'scans': row[4],
            'admin': row[5]
        }
        users.append(user)

    sqliteConnection.close()

    return jsonify(users)


'''
Function gets the user's remaining scans and permissions
Output: tuple [0] - remaining scans (int), [1] - is admin (bool)
'''
def get_scans(username: str):
    sqliteConnection = sqlite3.connect('sqlite.db')
    cursor = sqliteConnection.cursor()

    res = cursor.execute("SELECT scans, admin FROM users WHERE username = ?", (username, )).fetchone()

    print(res)

    sqliteConnection.commit()
    sqliteConnection.close()

    return res


'''
Function gets the user's remaining scans and permissions
Output: tuple [0] - remaining scans (int), [1] - is admin (bool)
'''
def resetPassword(username: str, phone: str):
    sqliteConnection = sqlite3.connect('sqlite.db')
    cursor = sqliteConnection.cursor()
    
    res = cursor.execute("SELECT phone FROM users WHERE username = ?", (username, )).fetchone()
    sqliteConnection.close()
    
    if res is None:
        return 'No username'
    
    if res[0] != phone:
        return 'phone number doesnt match'
    
    send_sms(phone)
    
    return 'sms sent'



'''
Function checks user credentials to the ones in the db
Output: string that says if the operation succeeded
'''
def updatePassword(username: str, password: str):
    sqliteConnection = sqlite3.connect('sqlite.db')
    cursor = sqliteConnection.cursor()
    
    hash = hashlib.md5()
    hash.update(password.encode("utf-8"))
    hash = hash.hexdigest()

    res = cursor.execute("UPDATE users SET password = ? WHERE username = ?", (hash, username)).fetchone()
    
    print(res)
    
    sqliteConnection.commit()
    sqliteConnection.close()

'''
Function sends email with the given verifecation code to the given mail address
Output: none
'''
def send_sms(receiver):
    verify.verifications.create(to=receiver, channel='sms')
    

if __name__ == "__main__":
    #socketio.run(app, debug=True)
    app.run(debug=True)

