import React, { useRef, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { toggleAction } from './login.component';
import '../loginform.css';

/*
Reset password form component
*/
const ResetPasswordForm = () => {

    const username = useRef(null);
    const phoneNumber = useRef(null);
    const newPassword = useRef(null);
    const verify = useRef(null);
    const [ver, setVer] = useState("reset");
    const [negVer, setNegVer] = useState("verify");

    // function that checks the action and its requirements and send the msg to the backend
    const submit = () => {

        fetch('http://localhost:5000/reset?username=' + username.current.value + '&phone=' + encodeURIComponent(phoneNumber.current.value)).then(
            res => res.json()
        ).then(
            data => {
                console.log(data.result)

                if (data.result === "sms sent")
                {
                    setVer((curr) => (curr === "reset" ? "verify" : "reset"));
                    setNegVer((curr) => (curr === "reset" ? "verify" : "reset"));
                }
            }
        )
        .catch(err => console.log(err))
        
    }

    // function that checks the action and its requirements and send the msg to the backend
    const confirm = () => {

        fetch('http://localhost:5000/confirm?user=' + username.current.value + '&pass=' + newPassword.current.value + '&code=' + verify.current.value + '&phone=' + encodeURIComponent(phoneNumber.current.value)).then(
            res => res.json()
        ).then(
            data => {
                console.log(data.result)

                if (data.result === "password reseted")
                {
                    window.location = "/login";
                }
                else {
                    toast.error("wrong verification code", {
                        position: toast.POSITION.TOP_CENTER
                    });
                }
            }
        )
        .catch(err => console.log(err))
    }



    return (
        <div className="web-login">
            <div className="title">
                <p align="center">Password Reset</p>
            </div>

            <ToastContainer />
            
            <form id={ver} className="loginForm">

                <input className="username" ref={username} type="text" placeholder="Username" />
                <input className="phone" ref={phoneNumber} type="tel" placeholder="e.g. +972549123123" />
                
                <div className="login-btn" onClick={submit}>
                    <p>Submit</p>
                </div >

            </form>

            <form id={negVer} className="loginForm">

                <input className="verifecation-code" ref={verify} type="number" placeholder="Enter code:" />
                <input className="new-pass" ref={newPassword} type="password" placeholder="New Password" />
                
                <div className="login-btn" onClick={confirm}>
                    <p>Submit</p>
                </div >

            </form>



            <div className="switch-action-btn">
                <p onClick={() => toggleAction('login')} >Switch to Log in</p>
                <p onClick={() => toggleAction('signup')} >Switch to Sign up</p>
            </div>

        </div>
    );
}

export default ResetPasswordForm;

