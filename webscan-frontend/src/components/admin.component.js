import React, { useState, useEffect } from 'react';
import '../App.css';
import '../widgets.css';


function Users() {
  const [users, setUsers] = useState([]);
  const [error, setError] = useState(null);
  const [editingUserId, setEditingUserId] = useState(null);
  const [editedUser, setEditedUser] = useState(null);

  useEffect(() => {
    fetch('http://localhost:5000/api/users')
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Received users:', data);
        setUsers(data);
        setError(null);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
        setError(error);
      });

  }, []);

  const handleEditUser = (event, user) => {
    event.preventDefault();
    setEditingUserId(user.id);
    setEditedUser(user);
  };

  const handleSaveUser = (event) => {
    event.preventDefault();
    fetch(`http://localhost:5000/api/users/${editedUser.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(editedUser),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to update user');
        }
        setEditingUserId(null);
        setUsers(users.map(u => (u.id === editedUser.id ? editedUser : u)));
      })
      .catch(error => {
        console.error('Error updating user:', error);
      });
  };

  const handleCancelEdit = (event) => {
    event.preventDefault();
    setEditingUserId(null);
    setEditedUser(null);
  };

  const handleFieldChange = (event) => {
    setEditedUser({ ...editedUser, [event.target.name]: event.target.value });
  };

  if (error) {
    return <div>Error fetching users: {error.message}</div>;
  }

  if (users.length === 0) {
    return <p>Loading...</p>;
  }

  return (

    <div style={{paddingTop: "20px"}}>
     <p style={{fontSize: "25px", textAlign: "center"}}>Users</p>
    <p>
      {users.map(user => (
        <li key={user.id} style={{position: "inherit", fontSize: "17px"}}>
          <br />
          <label>Name: {`${user.username}`}<br /></label>
          <label>Password: {`${user.password}`}<br /></label> 
          <label>Phone: {`${user.phone}`}<br /></label> 
          <label>Scans: {`${user.scans}`}<br /></label>
          <label>Admin: {`${user.admin}`}<br /></label>
          
          <button onClick={(event) => handleEditUser(event, user)}>Edit</button>
            {editingUserId === user.id && (
             <form onSubmit={handleSaveUser} onReset={handleCancelEdit} style={{ position: "absolute", top: "220px", left: "94px" , width: "500px"}}>
             
             <input type="text" name="username" id="username" value={editedUser.username} onChange={handleFieldChange} placeholder="e.g. john_doe" />
             <br />
             
             <input type="text" name="password" id="password" value={editedUser.password} onChange={handleFieldChange} placeholder="e.g. Pa$$w0rd" />
             <br />
             
             <input type="text" name="phone" id="phone" value={editedUser.phone} onChange={handleFieldChange} placeholder="e.g. 123-456-7890" />
             <br />
         
             <input type="number" name="scans" id="scans" value={editedUser.scans} onChange={handleFieldChange} placeholder="e.g. 5" />
             <br />
             
             <input type="number" name="admin" id="admin" value={editedUser.admin} onChange={handleFieldChange} placeholder="e.g. 0 or 1" />
             <br />
             <button type="submit">Save</button>
             <button onClick={handleCancelEdit}>Cancel</button>
           </form>)}
        </li>
      ))}
    </p>
  </div>
  );
}

const Admin = () => {
  return (
    <div>
     <div className='admin-panel'>
       <h1><Users /></h1>
    </div>
      
      
    </div>
  );
}

export default Admin;