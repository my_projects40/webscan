import React, { useRef } from "react";
import { toggleAction, createSessionToken } from './login.component';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../loginform.css';

const validPassword = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~!@#$%^&])[A-Za-z0-9~!@#$%^&]{8,}$"
);

/*
Sign up form component
*/
const SignupForm = () => {
    const username = useRef(null);
    const password = useRef(null);
    const secondPassword = useRef(null);
    const phoneNumber = useRef(null);

    // function that checks the action and its requirements and send the msg to the backend
    const sendMsg = () => {

        if (password.current.value !== secondPassword.current.value)
        {
            toast.error("passwords does not match!", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else if (!validPassword.test(password.current.value)) {
            toast.error("password requirements not match!", {
                position: toast.POSITION.TOP_CENTER
            });
        }

        else {
            let url = 'http://localhost:5000/signup?username=' + username.current.value + '&password=' + password.current.value + '&phone=' + encodeURIComponent(phoneNumber.current.value)
            console.log(url)
            fetch(url).then(
                res => res.json()
            ).then(
                data => {
                    console.log(data.msg)
                    if (data.msg === "User added") {
                        createSessionToken(username.current.value, false);
                        window.location = '/dashboard';
                    }
                    else {
                        window.location = '/signup';
                    }
                }
            )
            .catch(err => console.log(err))
        }
        
    }

    return (
        <div className="web-signup" style={{"height": "40em"}}>
            <div className="title">
                <p align="center">Sign up</p>
            </div>
            
            <ToastContainer />

            <form className="loginForm">

                <input className="username" ref={username} type="text" placeholder="Username" />
                <input className="password" ref={password} type="password" placeholder="Password" />
                <input className="verify-password" ref={secondPassword} type="password" placeholder="Verify Password" />
                <input className="phone-number" ref={phoneNumber} type="text" placeholder="Phone Number" />

                <div className="password-req">*Password requirements:<br/>
                    length is bigger than 8<br/> 
                    must contain 1 upper char, 1 lower char, 1 digit and 1 special char</div>

                <div className="login-btn" onClick={sendMsg}>
                    <p>Signup</p>
                </div >

                <div className="switch-action-btn" onClick={() => toggleAction('login')}>
                    <p>Switch to Log in</p>
                </div>

                <div className="forgot-pwd-btn" onClick={() => toggleAction('resetPwd')}>
                    <p>Forgot your password?</p>
                </div>

            </form>
        </div>
    );
}

export default SignupForm;

