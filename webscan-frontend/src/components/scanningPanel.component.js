import '../dashboard.css';
import React, { useState, useEffect, useCallback } from 'react';
import io from 'socket.io-client';
import '../loginform.css';
import * as ReactDOM from 'react-dom/client';
import { memo } from 'react';

const socket = io('http://localhost:5000');

let nextId = 0;
var container = null;
var panel = null;


function updatePanel(panel, dataArray) {
  const element = <ul>{dataArray.map(data => (<li key={data.id}>{data.info}</li>))}</ul>

  panel.render(element);
}

function ScanningPanel() {
  const queryParameters = new URLSearchParams(window.location.search);

  const url = queryParameters.get("url");
  const [dataArray, setDataArray] = useState([]);

  const addToPanel = (data) => {
    dataArray.push({
      id: nextId++,
      info: data
    });

    updatePanel(panel, dataArray);
  }

  const handleOnConnect = useCallback((data) => {
    console.log(data);
  }, []);

  const handleOnGetData = useCallback((data) => {
    addToPanel(data);
  }, []);
  
  const handleOnDone = useCallback((data) => {
    addToPanel(data);

    let scanJson = JSON.parse(data.substring(1, data.length - 1).replaceAll("'", '"'));
    let vulnerabilities = 0;
    let confiremd_vulnerabilities = 0;

    for (let i in scanJson)
    {
      vulnerabilities += 1;
      confiremd_vulnerabilities += scanJson[i];
    }

    console.log('done!');

    let finalData = 'Result: ' + Math.round(confiremd_vulnerabilities * (100 / vulnerabilities)) + ' out of 100';
    addToPanel(finalData);
  }, []);

  useEffect(() => {
    console.log("one time please!");
    
    container = document.getElementById('panel');
    panel = ReactDOM.createRoot(container);

    socket.on('connect', handleOnConnect);

    socket.emit('start_scan', url);

    socket.on('data', handleOnGetData);
    socket.on('done', handleOnDone);

    return () => {
      socket.off('connect', handleOnConnect);
      socket.off('data', handleOnGetData);
      socket.off('done', handleOnDone);
    }
  }, []);


  return (
    <div id='panel' className='scanning-panel'>
      <label>Scanning Panel</label>
    </div>
  );
}

export default memo(ScanningPanel);

