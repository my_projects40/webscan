import React, { Component, useRef } from "react";
import Cookies from 'universal-cookie';
import '../loginform.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


/*
redirect function
*/
export function toggleAction(page)
{
    window.location = '/' + page;
}

export function createSessionToken(username, isAdmin) {

    fetch('http://localhost:5000/getInfo?username=' + username).then(
            res => res.json()
        ).then(
            data => {
                var token = {
                    admin: isAdmin,
                    username: username,
                    scans: data.counter
                };

                let tokenStr = JSON.stringify(token);
            
                // creates the cookie of the user
                const cookies = new Cookies();
            
                cookies.set('session', btoa(tokenStr), { path: '/' });
            }
        )
        .catch(err => console.log(err))
}

/*
Log in form component
*/
const LoginForm = () => {

    const username = useRef(null);
    const password = useRef(null);
    // function that checks the action and its requirements and send the msg to the backend
    const sendMsg = () => {
        fetch('http://localhost:5000/login?username=' + username.current.value + '&password=' + password.current.value).then(
            res => res.json()
        ).then(
            data => {
                console.log(data.msg)
                if (data.msg === "User valid" || data.msg === "Admin") {
                    createSessionToken(username.current.value, data.msg === "Admin");
                    window.location = '/dashboard';
                    
                }
                else {
                    toast.error("password or username does not match!", {
                        position: toast.POSITION.TOP_CENTER
                        
                    });
                }
            }
        )
        .catch(err => console.log(err))
    }
    return (
        <div className="web-login">

            <div className="title">
                <p align="center">Log in</p>
            </div>

            <ToastContainer />
            
            <form className="loginForm">

                <input className="username" ref={username} type="text" placeholder="Username" />
                <input className="password" ref={password} type="password" placeholder="Password" />
                
                <div className="login-btn" onClick={sendMsg}>
                    <p>Login</p>
                </div >

                <div className="switch-action-btn" onClick={() => toggleAction('signup')}>
                    <p>Switch to Sign up</p>
                </div>

                <div className="forgot-pwd-btn" onClick={() => toggleAction('resetPwd')}>
                    <p>Forgot your password?</p>
                </div>

            </form>

        </div>
    );
}

export default LoginForm;

