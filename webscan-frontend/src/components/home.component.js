import React, { useRef } from "react";
import '../App.css';


const HomeForm = () => {

  return (
    <div>
      <div className='text3'>
          ZaHav <br />
      </div>
        <div className='text1'>
          ZaHav's Web <br />
          Vulnerability <br /> Scanner
      </div>

      <div className='text2'>
        Finds the vulnerabilities that put your web applications at <br />risk of attack.
        We will help you to eliminate the vulnerabilities.
      </div>
    </div>
  );
}

export default HomeForm;

