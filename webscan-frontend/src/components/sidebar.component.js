import React, { useState, useEffect } from 'react';
import { Sidebar, Menu, MenuItem, useProSidebar, SubMenu } from 'react-pro-sidebar';
import { Link } from 'react-router-dom';
import { AiFillHome, AiFillInfoCircle } from "react-icons/ai";
import Cookies from 'universal-cookie';


function Layout() {
  
  const { collapseSidebar } = useProSidebar();
  const cookies = new Cookies();

  const getMenuItems = () => {

    if (cookies.get("session") === "eyJhZG1pbiI6dHJ1ZSwidXNlcm5hbWUiOiJuaWtpMTIzIiwic2NhbnMiOjEwMDB9") {
        return [
            <MenuItem onClick={() => window.location = "/admin"}>
            <Link to="/admin"/>
                Admin
            </MenuItem>,
            <MenuItem onClick={() => window.location = "/dashboard"}>
            <Link to="/dashboard"/>
                Dashboard
            </MenuItem>,
            <MenuItem onClick={() => window.location = "/scan"}>
            <Link to="/scan"/>
                Scannig Panel
            </MenuItem>,
            <MenuItem key="logout" onClick={() => {
                cookies.remove("session");
            window.location = "/login";}}>
                Logout
           </MenuItem>
        ];
    }
    else if (cookies.get("session")) {
        return [
            <MenuItem onClick={() => window.location = "/dashboard"}>
            <Link to="/dashboard"/>
                Dashboard
            </MenuItem>,
            <MenuItem onClick={() => window.location = "/scan"}>
            <Link to="/scan"/>
                Scannig Panel
            </MenuItem>,
            <MenuItem key="logout" onClick={() => {
                cookies.remove("session");
            window.location = "/login";}}>
                Logout
           </MenuItem>
        ];
    }
     else {
        return [
            <MenuItem icon={<AiFillHome /> } onClick={() => window.location = "/"}>
            <Link to="/"/>
                Home
            </MenuItem>,
            <MenuItem onClick={() => window.location = "/login"}>
                <Link to="/login"/>
                Log In
            </MenuItem>,
            <MenuItem onClick={() => window.location = "/signup"}>
                <Link to="/signup"/>
                Sign Up
            </MenuItem>,
            <MenuItem onClick={() => window.location = "/resetPwd"}>
                <Link to="/resetPwd"/>
                Reset Password
            </MenuItem>
        ];
      }
  }

  
  return (
    <div className="web-sidebar">
        <Sidebar> 
            <Menu>
            {getMenuItems()}
            </Menu>
        </Sidebar>
        <main>
            <button onClick={() => collapseSidebar()}>
                <img src="logo192.png" width="23" height="20"/>
            </button>
        </main>
    </div>
    );
  
}



export default Layout;