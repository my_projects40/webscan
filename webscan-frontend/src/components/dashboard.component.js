import React, { useState, useRef, useEffect } from "react";
import Cookies from 'universal-cookie';
import '../dashboard.css';


const UserDashBoard = () => {

    const url = useRef(null);
    const cookies = new Cookies();
    const [scans, setScans] = useState(0);
    const [username, setUsername] = useState("");
    const [isAdmin, setAdmin] = useState(false);
    
    const submit = () =>
    {
        console.log(url.current.value);
        window.location.href = 'http://localhost:3000/scan?url=${url.current.value}';
        window.location = '/scan';
    }

    useEffect(() => {
        if (cookies.getAll()['session'] === undefined)
        {
            window.location = '/login';
        }


        var token = JSON.parse(atob(cookies.get('session')));

        setAdmin(token.admin);
        setScans(token.scans);
        setUsername(token.username);
    }, [])
   
    return (
        <div className="web-dashboard">

            <div className="username-show">
                <label>Username:<br/>{`${username}`}</label>
            </div>

            <div className="scans-count">
                <label>Scans:<br/>{`${scans}`}</label>
            </div>

            <div className="is-admin">
                <label>Is Admin:<br/>{`${isAdmin}`}</label>
            </div>

            <form className="check-dos" onSubmit={submit}>

                <input className="url" ref={url} type="text" placeholder="URL" />

                <button className="submit-scan-btn" type="submit">Start Scanning</button>

            </form>

        </div>
    );
}

export default UserDashBoard;