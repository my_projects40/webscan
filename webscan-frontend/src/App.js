import './App.css';
import 'boxicons/css/boxicons.min.css';
import ReactSwitch from 'react-switch';
import Sidebar from './components/sidebar.component';
import LoginForm from './components/login.component';
import SignupForm from './components/signup.component';
import HomeForm from './components/home.component';
import Admin from './components/admin.component';
import ResetPasswordForm from './components/resetpassword.component';
import UserDashBoard from './components/dashboard.component';
import ScanningPanel from './components/scanningPanel.component';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { useState } from 'react';

/**
 * 
 * function Home(content) {
  return (
    <div className='Home'>
      <p>{`${content.content}`}</p>
    </div>
  );
}
 */




function HelloMsg() {

  const cookies = new Cookies();

  const content = atob(cookies.get('session'));

  return (
    <div className='Home'>
      <p>{`Welcome ${content}!`}</p>
    </div>
  );
}

function App() {

  const cookies = new Cookies();

  if (cookies.getAll()['background'] === undefined)
  {
    cookies.set('background', 'light')
  }

  // Declare the state variables
  const [theme, setTheme] = useState(cookies.get('background')); // the theme (light or dark)
  
  // function switch the theme
  const toggleTheme = () => {
    cookies.set('background', (theme === "light" ? "dark" : "light"));
    setTheme(cookies.get('background'));
  }
  
  return (
    <div className='web-container' id={theme}>
      
      <Router>
        <Sidebar />
        
        <Routes>

          <Route path="/" exact element={<HomeForm />} />
          <Route path="/login" exact element={<LoginForm />} />
          <Route path="/signup" exact element={<SignupForm />} />
          <Route path="/resetPwd" exact element={<ResetPasswordForm />} />
          <Route path='/login/succeeded' exact element={<HelloMsg />} />
          <Route path='/signup/succeeded' exact element={<HelloMsg />} />
          <Route path='/dashboard' exact element={<UserDashBoard />} />
          <Route path='/admin' exact element={<Admin />} />
          <Route path='/scan' exact element={<ScanningPanel />} />
        </Routes>
      </Router>

      
      <div className='switch'>
        <ReactSwitch onChange={toggleTheme} checked={theme === "dark"}/>
        <label>{theme === "light" ? "Light Mode" : "Dark Mode"}</label>
      </div>
          
    </div>
  );
}

export default App;





