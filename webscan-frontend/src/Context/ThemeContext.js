import React, { createContext, useState } from 'react'

export const ThemeContext = createContext()

const ThemeContextProvider = (props) => {

    // Declare the state variables
    const [theme, setTheme] = useState("light"); // the theme (light or dark)
  
    // function switch the theme
    const toggleTheme = () => {
      setTheme((curr) => (curr === "light" ? "dark" : "light"));
    }

    return (
         <ThemeContext.Provider 
            value={{
                theme,
                toggleTheme
             }}>
               {props.children}
         </ThemeContext.Provider>
    )
}

export default ThemeContextProvider